declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "@coscine/api-connection";
declare module "@coscine/app-util";
declare module "@coscine/component-library";
